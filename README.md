# An Example Project for stele1

This is a tiny, complete [stele1 project](https://stele1.steleclimbing.org)
for a climbing area in the suburbs north of Washington DC.

The area, a single rock called the Eden Brook Boulder,
is documented almost entirely in [one video](https://www.youtube.com/watch?v=KqYT-8O0p90).

## Overview

- [`eden-brook.stele1.d/`](eden-brook.stele1.d/) contains the stele1 project itself.
The data is stored in straightforward files.
- for example
  - the climb _Tuesday Trivia_ has the UUID
    <small></code>c38e2055-647e-479f-affa-11e46d4e3b67</code></small>
    [this file](eden-brook.stele1.d/climbs/c38e2055-647e-479f-affa-11e46d4e3b67.json).
  - [this JPEG](eden-brook.stele1.d/photos/51657e5b-7b0d-402c-b6bb-b9c356a80624_image.jpg)
    has a
    [sidecar file](eden-brook.stele1.d/photos/51657e5b-7b0d-402c-b6bb-b9c356a80624.json)
    that holds the metadata.
- see the
  [reference docs](http://stele1.steleclimbing.org/reference/)
  for details on the data format.
- [`./context/`](./context/)
  has extra info about the climbing area.

<!--
![an overview of this stele1 minimal example project](intro.png)

- [_Build a Website_](./generate-a-static-site/)
  uses Python to build a static website
- [_Build a Single Page Application_](./generate-a-web-app/)
  uses JavaScript to build a lightweight SPA
- [_Build a PDF_](./generate-a-pdf/)
  uses *nix tools to generate a small PDF guide 
- [_Perpare for Long Term Storage_](./prepare-for-long-term-storage/)
  uses *nix tools to help you climbing data last long into the future
-->

--------------------------------------------------------------------------------

All content (except from [this pdf](context/the-original-eden-brook-guide.pdf))
in available under the [CC0 License](https://creativecommons.org/publicdomain/zero/1.0/).
